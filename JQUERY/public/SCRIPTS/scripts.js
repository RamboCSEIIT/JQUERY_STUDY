



var myApp = (function () {

    var now = new Date();

    return {

        doSomething: function ()
        {
            'use strict';

            var myNumber = 181;
            //var i = 0;
            var totalFunctionsCalled = 0;
            var myArray = ['Hank', 'Jesse', 'Walter'];
            myNumber++;
            console.log('myNumber equals  ::--:: ' +  myNumber);

            function firstFunction() {
                 console.log('firstFunction');
                totalFunctionsCalled++;
                secondFunction();
               
            }

            function secondFunction() {
                 console.log('secondFunction');
                totalFunctionsCalled++;
                thirdFunction();
            }

            function thirdFunction() {
                 console.log('thirdFunction');
                totalFunctionsCalled++;
                fourthFunction();
            }

            function fourthFunction() {
                 console.log('fourthFunction');
                totalFunctionsCalled++;
            }

            // call firstFunction; start the chain of events
            firstFunction();

        }
    };
})();

myApp.doSomething();

 